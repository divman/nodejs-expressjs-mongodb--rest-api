import mongoose from 'mongoose'
import beautifyUnique from 'mongoose-beautiful-unique-validation'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'

import constants from '../../config/constants'

const Schema = mongoose.Schema

const UserSchema = new Schema({
  employeeid: {
    type: String,
    unique: true,
    required: [true, 'Employee Id is required!'],
    trim: true
  },
  email: {
    type: String,
    unique: true,
    required: [true, 'Email is required!'],
    trim: true,
    validate: {
      validator(email) {
        const emailRegex = /^[-a-z0-9%S_+]+(\.[-a-z0-9%S_+]+)*@(?:[a-z0-9-]{1,63}\.){1,125}[a-z]{2,63}$/i;
        return emailRegex.test(email)
      },
      message: '{VALUE} is not a valid email!'
    }
  },
  username: {
    type: String,
    unique: true,
    required: [true, 'Username is required'],
    trim: true
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    validate: {
      validator(password) {
        return password.length >= 3 //&& password.match(/\d+/g)
      },
      message: 'Not a valid password!'
    }
  }
})

UserSchema.plugin(beautifyUnique, {
  defaultMessage: '({VALUE}) already exists in system'
})

UserSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = this.hashPassword(this.password)
    return next()
  }

  return next()
})

UserSchema.methods = {
  hashPassword(password) {
    return bcrypt.hashSync(password)
  },

  comparePassword(password) {
    return bcrypt.compareSync(password, this.password)
  },

  createJWTtoken() {
    const THIRTY_MINUTES = 60 * 30
    return jwt.sign({ _id: this._id },
      constants.tokenSecret,
      { expiresIn: THIRTY_MINUTES })
  },

  authUserJSON() {
    return {
      token: this.createJWTtoken(),
      authorizedUser: this.userObjJSON()
    }
  },

  userObjJSON() {
    return {
      _id: this._id,
      employeeid: this.employeeid,
      username: this.username,
      email: this.email
    }
  }
}

export default mongoose.model('User', UserSchema)
