import UserService from './user-service'

export const signup = async (req, res) => {
  try {
    const user = await UserService.register(req.body)
    return res.status(200).json(user)
  } catch (error) {
    return res.status(400).json({
      status: 'failure',
      error: String(error)
    })
  }
}

export const login = (req, res, next) => {
  res.status(200).json(req.user.authUserJSON())
  return next()
}

export const getuser = (req, res, next) => {
  res.status(200).json({
    error_code: 0,
    status: "success",
    user: req.user.userObjJSON()
  })
  return next()
}
