import Joi from '@hapi/joi'

export const passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/

const userJoiValidation = {
  validateBody: (schema) => {
    return (req, res, next) => {
      const result = Joi.validate(req.body, schema)
      if (result.error) {
        return res.status(400).json({
          error_code: 1,
          status: "failure",
          error: result.error.details[0].message
        })
      } else {
        next()
      }
    }
  },

  schemas: {
    userJoiBody: Joi.object({
      employeeid: Joi.string().required(),
      email: Joi.string().email().required(),
      username: Joi.string().required(),
      password: Joi.string().regex(passwordRegex).required()
    })
  }
}

export default {
  ...userJoiValidation
}
