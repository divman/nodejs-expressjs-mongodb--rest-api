import UserModel from './user-model'
import { userAuthLocal } from '../../config/passport'
import { userAuthJWT } from '../../config/passport'
import { isAuthenticated } from '../../config/passport'

class UserService {
  register({ employeeid, email, username, password }) {
    if (!employeeid) {
      throw new Error('Employee Id is required')
    } else if (!email) {
      throw new Error('Email is required')
    } else if (!username) {
      throw new Error('Username is required')
    } else if (!password) {
      throw new Error('Password is required')
    }

    try {
      return UserModel.create({ employeeid, email, username, password })
    } catch (error) {
      throw error
    }
  }

  userLogin(req, res, next) {
    return userAuthLocal(req, res, next)
  }

  userAuthenticate(req, res, next) {
    return userAuthJWT(req, res, next)
  }

  isAuthenticated(req, res, next) {
    return isAuthenticated(req, res, next)
  }
}

export default new UserService()
