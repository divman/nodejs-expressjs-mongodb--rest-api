import express from 'express'
// import validate from 'express-validation'
// const validator = require('express-joi-validation')({ passError: true })

import * as userController from './user-controller'
import validateUserRequest from './user-validations'
import UserServices from './user-service'

const routes = express.Router()

routes.post('/register',
  validateUserRequest.validateBody(validateUserRequest.schemas.userJoiBody),
  userController.signup)

routes.post('/login',
  UserServices.userLogin,
  userController.login)

// routes.get('/testing', UserServices.userAuthenticate, (req, res) => {
//   res.send('Auth testing passed user authentication...')
// })

routes.get('/testing',
  UserServices.isAuthenticated,
  userController.getuser)

export default routes
