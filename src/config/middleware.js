import bodyParser from 'body-parser'
// import cors from 'cors'

const isDev = process.env.NODE_ENV === 'development'

export default (app) => {
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  // app.use(cors())

  if (isDev) {
    const morgan = require('morgan')
    app.use(morgan('dev'))
  }
}
