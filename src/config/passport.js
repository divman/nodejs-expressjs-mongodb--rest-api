import passport from 'passport'
import LocalStrategy from 'passport-local'
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt'

import constants from './constants'
import UserModel from '../modules/user/user-model'

const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
  secretOrKey: constants.tokenSecret
}

const jwtLogin = new JWTStrategy(jwtOpts, async (payload, done) => {
  try {
    const user = await UserModel.findById(payload._id)

    if (!user) {
      return done(null, false)
    }

    return done(null, user)
  } catch (error) {
    return done(error, false)
  }
})

const localOpts = {
  usernameField: 'email'
}

const localLogin = new LocalStrategy(localOpts, async (email, password, done) => {
  try {
    const user = await UserModel.findOne({ email })

    if (!user) {
      return done(null, false)
    } else if (!user.comparePassword(password)) {
      return done(null, false)
    }

    return done(null, user)
  } catch (error) {
    return done(error, false)
  }
})

const isAuthenticate = async (req, res, next) => {
  await passport.authenticate('jwt', (err, user) => {
    if (err || !user) {
      res.status(403).json({
        error_code: 1,
        status: 'failure',
        error: 'You do not have access to this resource. Please login to the system.'
      })
    } else {
      req.user = user
      next()
    }
  })(req, res, next)
}

passport.use(jwtLogin)
passport.use(localLogin)

export const userAuthJWT = passport.authenticate('jwt', { session: false })
export const userAuthLocal = passport.authenticate('local', { session: false })
export const isAuthenticated = isAuthenticate
