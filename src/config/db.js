import mongoose from 'mongoose'
import constants from './constants'

mongoose.Promise = global.Promise
let connURL = constants.DB_URL

// try {
//   mongoose.connect(connURL, {
//     useMongoClient: true
//   })
// } catch (error) {
//   mongoose.createConnection(connURL, {
//     useMongoClient: true
//   })
// }

try {
  mongoose.connect(connURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
} catch (error) {
  mongoose.createConnection(connURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
}

mongoose.connection
  .once('open', () => {
    console.log('MongoDB Running Successfully...')
  })
  .on('error', (err) => {
    throw err
  })
