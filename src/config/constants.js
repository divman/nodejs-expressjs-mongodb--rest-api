const portConfig = {
  // PORT: process.env.PORT || 0000,
  // tokenSecret: process.env.JWT_SECRET || "secretToken"
  PORT: process.env.PORT,
  tokenSecret: process.env.JWT_SECRET
}

const DB = process.env.MONGODB_CONN.replace('<PASSWORD>', process.env.MONGODB_PWD);

const dbConfig = {
  development: {
    DB_URL: DB
  },
  production: {
    DB_URL: 'mongodb://localhost/hrmDB'
  }
}

function getEnv(env) {
  return dbConfig[env]
}

export default {
  ...portConfig,
  ...getEnv(process.env.NODE_ENV)
}
