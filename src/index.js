import '../env';

import express from 'express';
import './config/db';
import middlewares from './config/middleware';
import userRoutes from './modules/user'

const app = express();
const port = process.env.PORT;

middlewares(app);

app.use('/user', userRoutes);

app.get('/', (req, res) => {
  res.send({
    status: 'success',
    message: `Server started on port ${port} `
  });
});

app.listen(port, (err) => {
  if (err) throw err;
  console.log(`Node server started on ${port}.`);
});
